#!/bin/bash

BACKUP=/usr/AGL/cluster-demo/.cluster
mkdir -p $BACKUP

if [ -e $BACKUP/weston.ini ]; then
    mv $BACKUP/weston.ini /etc/xdg/weston/weston.ini
fi

if [ -e $BACKUP/weston.service ]; then
    mv $BACKUP/weston.service /lib/systemd/system/weston.service
fi

rm /usr/lib/libmmngrbuf.so.1 > /dev/null 2>&1
