#!/bin/bash

BACKUP=/usr/AGL/cluster-demo/.cluster
mkdir -p $BACKUP

if [ ! -e $BACKUP/weston.ini ]; then
    cp /etc/xdg/weston/weston.ini $BACKUP/weston.ini
fi

if [ ! -e $BACKUP/weston.service ]; then
    cp /lib/systemd/system/weston.service $BACKUP/weston.service
fi

cp /usr/AGL/cluster-demo/weston.ini.cluster-demo /etc/xdg/weston/weston.ini
cp /usr/AGL/cluster-demo/weston.service.cluster-demo /lib/systemd/system/weston.service

ln -fs /usr/local/lib/libmmngrbuf.so.1 /usr/lib/libmmngrbuf.so.1
